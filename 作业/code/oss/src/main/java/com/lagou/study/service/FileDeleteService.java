package com.lagou.study.service;

import com.aliyun.oss.OSSClient;
import com.lagou.study.config.AliyunConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class FileDeleteService {
    @Autowired
    private AliyunConfig aliyunConfig;

    @Autowired
    private OSSClient ossClient;

    public void delete(String objectName) {
        ossClient.deleteObject(aliyunConfig.getBucketName(), objectName);
        ossClient.shutdown();
    }
}
