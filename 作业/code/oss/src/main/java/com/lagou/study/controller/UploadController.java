package com.lagou.study.controller;

import com.lagou.study.pojo.UploadResult;
import com.lagou.study.service.FileUploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

@RequestMapping("/oss")
@Controller
public class UploadController {

    @Autowired
    private FileUploadService fileUpLoadService;

    @PostMapping("/upload")
    @ResponseBody
    public UploadResult upload(@RequestParam("file") MultipartFile multipartFile) {
        return fileUpLoadService.upload(multipartFile);
    }
}