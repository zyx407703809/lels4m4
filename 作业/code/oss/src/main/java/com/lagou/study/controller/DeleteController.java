package com.lagou.study.controller;

import com.lagou.study.service.FileDeleteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@RequestMapping("/oss")
@Controller
public class DeleteController {
    @Autowired
    private FileDeleteService fileDeleteService;

    @PostMapping("/delete")
    @ResponseBody
    public void upload(@RequestParam("filename") String objectName) {
        fileDeleteService.delete(objectName);
    }
}
