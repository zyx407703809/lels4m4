package com.lagou.study.controller;

import com.lagou.study.service.FileDownloadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@RequestMapping("/oss")
@Controller
public class DownloadController {
    @Autowired
    private FileDownloadService fileDownloadService;

    @PostMapping("/download")
    @ResponseBody
    public void upload(@RequestParam("filename") String objectName) {
        fileDownloadService.download(objectName);
    }
}
