package com.lagou.study.service;

import com.aliyun.oss.OSSClient;
import com.lagou.study.pojo.UploadResult;
import com.lagou.study.config.AliyunConfig;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.UUID;

@Service
public class FileUploadService {

    @Autowired
    private AliyunConfig aliyunConfig;

    @Autowired
    private OSSClient ossClient;

    // 允许上传的格式
    private static final String[] IMAGE_TYPE = new String[]{".jpg", ".jpeg", ".png"};

    public UploadResult upload(MultipartFile uploadFile) {
        // 校验图片格式
        boolean isLegal = false;
        for (String type : IMAGE_TYPE) {
            if (StringUtils.endsWithIgnoreCase(uploadFile.getOriginalFilename(), type)) {
                isLegal = true;
                break;
            }
        }

        UploadResult uploadResult = new UploadResult();
        if (!isLegal) {
            uploadResult.setStatus("图片必须为.jpg或.jpeg或.png");
            return uploadResult;
        }

        // 图片大小检查
        if(uploadFile.getSize()/1024/1024 > 5) {
            uploadResult.setStatus("文件不能大于5M");
            return uploadResult;
        }


        String fileName = uploadFile.getOriginalFilename();
        String filePath = getFilePath(fileName);

        try {
            ossClient.putObject(aliyunConfig.getBucketName(),filePath, new ByteArrayInputStream(uploadFile.getBytes()));
        } catch (IOException e) {
            e.printStackTrace();
            //上传失败
            uploadResult.setStatus("error");
            return uploadResult;
        }
        uploadResult.setStatus("done");
        uploadResult.setName(this.aliyunConfig.getUrlPrefix() + filePath);
        uploadResult.setUid(String.valueOf(System.currentTimeMillis()));
        return uploadResult;
    }

    private String getFilePath(String sourceFileName) {
        return UUID.randomUUID().toString() + "." + StringUtils.substringAfterLast(sourceFileName, ".");
    }
}