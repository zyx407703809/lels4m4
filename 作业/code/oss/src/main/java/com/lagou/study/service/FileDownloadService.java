package com.lagou.study.service;

import com.aliyun.oss.OSSClient;
import com.aliyun.oss.model.GetObjectRequest;
import com.lagou.study.config.AliyunConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.File;

@Service
public class FileDownloadService {

    @Autowired
    private AliyunConfig aliyunConfig;

    @Autowired
    private OSSClient ossClient;

    public void download(String objectName) {
        ossClient.getObject(new GetObjectRequest(aliyunConfig.getBucketName(), objectName), new File("/Users/zhaoyuxuan/Desktop/abc.png"));
        ossClient.shutdown();
    }
}
